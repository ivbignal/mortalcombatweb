<?php

function ErrorPage404()
{
    $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
    header('HTTP/1.1 404 Not Found');
	header("Status: 404 Not Found");
	header('Location:'.$host.'404');
}

$routes = explode('/', $_SERVER['REQUEST_URI']);

if (!empty($routes[1])) {
    $page_name = $routes[1];
}else{
    $page_name = "index";
}
    
if (!empty($routes[2])) {
    $action_name = $routes[2];
}else{
    $action_name = "index";
}

$controller_file = strtolower($page_name).'.php';
$controller_path = "controllers/".$controller_file;
if (file_exists($controller_path)) {
    include "controllers/".$controller_file;
} else {
    ErrorPage404();
}